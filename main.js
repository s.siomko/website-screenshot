const puppeteer = require("puppeteer");
const fs = require("fs");
const { url } = require("inspector");

(async () => {
  let { data } = await readJsonFile("urls.json");

  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  data.forEach(async (url) => {
    await page.goto(url);
    let siteName = url.split(".").reverse()[1];
    console.log(siteName);
    url.resolutions.forEach(async ({ width, height = 1000 }) => {
      console.log(`${siteName}-${width}-${height}-${Date.now()}`);
      //await page.screenshot({ path: `` });
    });
  });

  await browser.close();
})();

async function readJsonFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      if (err) {
        reject(err.message);
        console.log(err);
      }

      resolve(JSON.parse(data));
    });
  });
}
